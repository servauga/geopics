/*jslint node: true */
/*global console, ol, Template, Meteor,tabPics, currentPics, global */
/*jslint nomen: true */


indexCurrentPics=0;
currentPics="";
Session.set('creatingBook',false);
Session.set('displayingBooks',false);
Session.set('displayingOneBook',false);
Session.set('addingPhotos',false);
Session.set('currentlyReadPics',"");

function Book(name, user, description){
  this.name = name,
  this.user = user,
  this.description = description,
  this.URLcover = "",
  this.URLcoverThumb = "",
  this.photos = []
};

function updateFileId(id){
  this.file_id = id;
};

function updateURLCover(url){
  this.URLcover = url;
};
function updateURLCoverThumb(url){
  this.URLcoverThumb = url;
};
Template.upload.events({
    'submit .form-upload' : function (e) {

        e.preventDefault();
        var inputfiles = e.target.filesToUpload.files,
            tab = Session.get('uploadedEXIF');

        for(var i=0; i < inputfiles.length ; i++){
          var file = inputfiles[i];
          var fileObj = Files.insert(file);
          Thumbs.insert(file);
          updateFileId.call(tab[i], fileObj._id);
          //TODO METEOR METHODS SERVER SIDE
          Session.set('uploadedEXIF',tab);
          tab[i].localURL="";
          var id = DBPhotos.insert(tab[i]);
          //Remplissage de book
          Books.update({_id:Session.get('idCurrentBook')}, {$push : {photos : id}});

        }
        global.resetTables();
        Session.set('idCurrentBook',"");
        Session.set('addingPhotos',false);
    },
    'click #cancel' : function() {
      global.resetTables();
      Session.set('displayingOneBook',false);
      Session.set('creatingBook', false);
      Session.set('addingPhotos',false);
    },
    //When adding pictures to Book
    'change #filesToUpload' : function(e){
        e.preventDefault();
        global.resetTables();
        global.getAllEXIF(e);
    },
    //Click down on one of the thumbnails
    'click #thumb' : function(e){
      //Set Center of the selected picture
      var coordPics = global.convertStringToCoord(e.target.alt);
      global.map.getView().setCenter(coordPics);
      //Selection of the clicked picture on the map
      var id = global.tmpFeatures.getClosestFeatureToCoordinate(coordPics).getId();
      global.select.getFeatures().clear();
      global.select.getFeatures().push(global.tmpFeatures.getFeatureById(id));
      Session.set("currentPics",id);
      //Add calendar with picture s date

      var tab = Session.get('uploadedEXIF');
      //Delete class for elements highlighted
      if(document.getElementsByClassName('img-circle').length !== 0){
        document.getElementsByClassName('img-circle')[0].className="";
      }
      //Add highlight
      e.target.className ="img-circle";
      for(var i = 0;i < tab.length;i++){
        if(tab[i].name === Session.get("currentPics")){
          $('#datetimepicker1').data("DateTimePicker").setDate(tab[i].date);
        }
      }
    },
    //validation changes pictures properties
    'submit .change': function(event){
      event.preventDefault();
      var pic = global.getCurrentPics(false);
      console.log($('#datetimepicker1').data("DateTimePicker"));
      console.log($('#datetimepicker1').data("DateTimePicker").getDate());
      global.updatePhoto(event.target.name.value, event.target.desc.value, $('#datetimepicker1').data("DateTimePicker").getDate()._d);
      Session.set('currentPics',"");
    },
    'click #newbook' : function(){
          if(Session.get('displayingBooks')){
            Session.set('displayingBooks', false);
          }
          if(!Session.get('addingPhotos')){
            global.resetTables();
            Session.set('displayingOneBook',false);
            Session.set('creatingBook', !Session.get('creatingBook'));
          }
    },
    'click #mybooks' : function(){

      if(Session.get('creatingBook')){
        Session.set('creatingBook', false);
      }
      if(!_.isEmpty(global.select)){
        global.select.getFeatures().clear();
      };
      Session.set('addingPhotos',false);
      Session.set('displayingOneBook',false);
      Session.set('displayingBooks', !Session.get('displayingBooks'));
    },
    //Creation of new book
    'submit #book-creation' : function(event){
      event.preventDefault();
      var title = event.target.name.value,
        description = event.target.description.value,
        bookcover = event.target.bookCover.files[0],

        //TODO MODIFY BOOKS CREATION ONCE AUTOPUBLISH REMOVED
        newBook = new Book(title, 'Bob', description);

      //If a coverBook is added ==> Update Book object
      if(bookcover !== undefined){
        var fileObj = Files.insert(bookcover);
        var thumb = Thumbs.insert(bookcover);
        updateURLCover.call(newBook, fileObj._id);
        updateURLCoverThumb.call(newBook, thumb._id);
      };

      var insertedBook = Books.insert(newBook);
      Session.set('idCurrentBook', insertedBook);
      Session.set('creatingBook', !Session.get('creatingBook'));
      Session.set('addingPhotos',true);

    },
    //Delete textarea in input when user click on it
    'click .input': function(e){
      e.target.value = "";
    },
    'click .menu-books' : function(e){
        Session.set('displayingOneBook',true);
        Session.set('creatingBook',false);
        Session.set('displayingBooks',false);
        Session.set('addingPhotos',false);
        Session.set('idCurrentBook',e.target.id);
        //Add markers on map
        var book = Books.findOne({_id:e.target.id});
          var photos = book.photos,
          allPhotos=[];
        photos.forEach(function(photo){
              allPhotos.push(DBPhotos.findOne({_id:photo}));
        });
        global.convert2Features(allPhotos);
        Session.set('allPhotos',allPhotos);
    },
    'click #go-back' : function(e){
        Session.set('displayingBooks',false);
      },
      'change #bookCover' : function(e){
        if(e.target.files.length === 0){
          document.getElementById('name-cover').innerHTML = "My Cover : Default"
        } else {
          document.getElementById('name-cover').innerHTML = "My cover : " +e.target.files[0].name;
          $('#upload-button').removeClass('btn btn-info');
          $('#upload-button').addClass('btn btn-default');

        }
      }
});
