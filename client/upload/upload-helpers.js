/*jslint node: true */
/*global console, FS,dico, $,alert, Photos, Images, Session, OpenLayers, Template, Meteor, UsersCpt,Deps,currentPics, tabPics, addLocalURL */
/*jslint nomen: true */
"use strict";



function addLocalURL(url){
  this.localURL = url;
};

function updateCoverURL(url){
  this.URLcover = url;
};

Template.upload.helpers({
    Pics : function () {
        return Session.get('allPhotos');
    },
    url : function () {
        return Session.get('uploadedPics');
    },
    exif : function () {

        if(Session.get('uploadedPics')){
            if(Session.get('uploadedEXIF').length !== 0){
                var old = Session.get('uploadedEXIF');
                var urls = Session.get('uploadedPics');
                for(var i = 0; i< old.length;i++){
                  addLocalURL.call(old[i],urls[i].src);
                }
                Session.set('uploadedEXIF', old);
            }
        }
        return Session.get('uploadedEXIF');
    },
    metadata : function(){
      return global.getCurrentPics(false);
    },
    creatingBook : function(){
      return Session.get('creatingBook');
    },
    displayingBooks : function(){
      return Session.get('displayingBooks');
    },
    displayingOneBook : function(){
      return Session.get('displayingOneBook');
    },
    addingPhotos : function(){
      return Session.get('addingPhotos');
    },
    book : function(){
      return Books.findOne({_id:Session.get('idCurrentBook')});
    },
    mybooks : function(){
      var all_Books = [];
      for(var i=0;i<Books.find({user:"Bob"}).fetch().length;i++){

        var id_cover = Books.find({user:"Bob"}).fetch()[i].URLcover,
          id_thumb = Books.find({user:"Bob"}).fetch()[i].URLcoverThumb,
          id = Books.find({user:"Bob"}).fetch()[i]._id ;

              var file = Files.findOne({_id:id_cover});
              var fileThumb = Thumbs.findOne({_id:id_thumb});

              if(file !== undefined){
              var url = file.url(),
                urlThumb = fileThumb.url();
              Books.update({_id:id},{$set:{URLcover:url}});
              Books.update({_id:id},{$set:{URLcoverThumb:urlThumb}});

        } else if(id_cover[0] !== "/") {
          // Standard Icon for Book
          var test = Books.findOne({_id:id});
          Books.update({_id:id},{$set:{URLcover:"standardBookCover.png"}});
          Books.update({_id:id},{$set:{URLcoverThumb:"standardBookCover.png"}});
        }
      };
      var final = Books.find({user:"Bob"}).fetch(),
        length = final.length,
        save;
      if(final.length <15){
          for(var i = 0; i < length;i++){
            var essai = Math.floor(Math.random() * length);
            if(save === essai){
              essai = Math.floor(Math.random() * length);
            }
            save = essai;
            final.push(final[essai]);
          }
        };
      return final;

    },
    currentlyReadPics : function(){
      var photo = Session.get('currentlyReadPics');
      if(photo.localURL === ""){
          var file = Files.findOne({_id : photo.file_id});
          DBPhotos.update({_id:photo._id},{$set:{localURL:file.url()}});
          Session.set('currentlyReadPics',DBPhotos.findOne({_id:photo._id}));
      };
      return Session.get('currentlyReadPics');
    },
    currentBookCoverURL : function(){
      var currentBook = Books.findOne({_id:Session.get('idCurrentBook')}),
        cover;
      if(Thumbs.findOne({_id:currentBook.URLcoverThumb}) !== undefined){
      cover = Thumbs.findOne({_id:currentBook.URLcoverThumb}).url();
    }else{
      cover = "standardBookCover.png"
    }
      return cover;
    }
})
