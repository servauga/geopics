/*jslint node: true */
/*global console, ol, Template, Meteor,map */
/*jslint nomen: true */
'use strict';


///////////////////////////////////////////////////////////////////////
/////////////////////////VARIABLES GLOBALES////////////////////////////
///////////////////////////////////////////////////////////////////////
var draw,
    edit;

var init = function ()   {

    //Initialisation Map
    global.map = new ol.Map({
        target: 'div-map',
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM({layer: 'sat'})
            })
        ],
        view: new ol.View({
            center: ol.proj.transform([37.41, 8.82], 'EPSG:4326', 'EPSG:3857'),
            zoom: 4,
            minZoom : 5
        })
    });
};


//Initialisation Map during template rendering
Template.map.rendered = init;
