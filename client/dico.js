/*jslint node: true */
/* global dico, ol, Session */


function Photo(name, user, coordinates, date, description, camera, localURL){
    this.name = name,
    this.user = user,
    this.coordinates = coordinates,
    this.date = date,
    this.description = description,
    this.camera = camera,
    this.localURL = localURL,
    this.file_id = ""
};

function updateCoord(coord){
  this.coordinates = coord;
};
function updateNameAndDescription(name, desc,date){
  this.name = name;
  this.description = desc;
  this.date = date;
};

global = {
    map : {},//objet map openlayers
    featureOverlay : {},
    essai : {},
    tmpFeatures : {},
    modify : {},
    select : {}, //new ol.interaction.Select(),

    minuteToDegree : function(data) {
    if(data === undefined){
      return 0;
    }
		return (data[0] + data[1]/60 + data[2]/3600);
	},
    resetTables : function(){
      tab = [] ;
      Session.set('uploadedPics', tab);
      Session.set('uploadedEXIF', tab);

      //Test if global.select isEmpty
      //(otherwise raises error at init with newBook button)
      if(!_.isEmpty(global.select)){
        global.select.getFeatures().clear();
      };
      //TODO : Modify to prevent clipping on reset....
      global.map.getLayers().clear();
      global.map.addLayer(new ol.layer.Tile({
        source: new ol.source.OSM({layer: 'sat'})
      }));
    },
    tranformFromLatLon : function (latlon) {
        return ol.proj.transform([latlon[0], latlon[1]], 'EPSG:4326', 'EPSG:900913');
    },
    convertGPSdata : function (data) {
		var output = data,
		  latRef = {'N': 1, 'S': -1},
			lonRef = {'E': 1, 'W': -1};

    //Case no GPS Data
    if(output.GPSLatitude === undefined){
      return false;
    };
		output.GPSLatitude = latRef[data.GPSLatitudeRef] * global.minuteToDegree(output.GPSLatitude);
		output.GPSLongitude = lonRef[data.GPSLongitudeRef] * global.minuteToDegree(output.GPSLongitude);

		return output;
	},
    readURL: function(input) {
        var tab = []
		var files = input.files;
        var selDiv;
        var filesArr = Array.prototype.slice.call(files);

        filesArr.forEach(function(f) {
			if(!f.type.match("image.*")) {
				return;
			}

			var reader = new FileReader();
			reader.onload = function (e) {
				tab.push({'src':e.target.result});
                Session.set('uploadedPics',tab);
			}
            reader.readAsDataURL(f);
        });
    },
    getAllEXIF : function(e) {
      var tabPics = [];
      if( e.target.files.length !== 0 ){
          Session.set('inputFilled',true);
          global.readURL(e.target);
      }
      for(var i = 0; i< e.target.files.length; i++){

      (function(file){
        var file = file,
            file_path = file.name,
            coordinates = [],
            exif_converted,
            exif,
            tab=[];

        if(file_path){
            var startIndex = (file_path.indexOf('\\') >= 0 ? file_path.lastIndexOf('\\') : file_path.lastIndexOf('/'));
            var filename = file_path.substring(startIndex);
            if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
              filename = filename.substring(1);
            }
        }
        fr = new FileReader;
        fr.onloadend = function(e){

          exif = EXIF.readFromBinaryFile(new BinaryFile(e.target.result));

          exif_converted = global.convertGPSdata(exif);

          if(exif_converted !== false){
          coordinates = [exif_converted.GPSLongitude, exif_converted.GPSLatitude];
          coordinates = global.tranformFromLatLon(coordinates);
        } else { //Case NO GPS DATA
            coordinates = [0,0];
          }
          //TODO : change "bob" to "Meteor.user().profile.name" when user accounts set up //
          var date = global.parseDateAndTime(exif.DateTime);
          tabPics.push(new Photo(filename, "bob"/*Meteor.user().profile.name*/, coordinates, date/*exif.DateTime*/,
           "" , exif.MODEL, ""));
          Session.set('uploadedEXIF',tabPics);
          //Display Feature on Map
          global.convert2Features(undefined);
        }
        fr.readAsBinaryString(file);
      })(e.target.files[i]);

    }
  },
  //Convert picture to feature for map
  convert2Features : function(list_pics){
      var tmp_Pics = [];

      if(list_pics === undefined){
        list_pics = Session.get('uploadedEXIF');
        global.select = new ol.interaction.Select({condition : ol.events.condition.never});
        //Ajout evenement click//
        //Management event of select interaction
        global.select.getFeatures().on('change:length', function(e){

          if(global.select.getFeatures().length !== 0){

            if(e.target.item(0)){
              var feat = e.target.item(0);
              feat.on('change', function(){
                global.updateCoordinates(feat.getGeometry().getCoordinates());
              });
            }
          } else {
            Session.set("currentPics",{});
          }
        });
        //Fin evenement click//

      }else{
        global.select = new ol.interaction.Select();
        //Ajout evenement click//
        //Management event of select interaction
        global.select.getFeatures().on('change:length', function(e){

            if(e.target.item(0)){
              //Click on marker displays pics
              var coord = e.target.item(0).getGeometry().getCoordinates();
              for(i in Session.get('allPhotos')){
                if(Session.get('allPhotos')[i].coordinates[0] === coord[0] && Session.get('allPhotos')[i].coordinates[1] === coord[1])
                  Session.set("currentlyReadPics",Session.get('allPhotos')[i]);
              }
          } else {
            Session.set("currentlyReadPics","");
          }
        });
        //Fin evenement click//
      }
      if(list_pics){
        for(i in list_pics){

          var pic = list_pics[i],
          feat = new ol.Feature({
            'geometry':new ol.geom.Point(pic.coordinates),
            'name': pic.name
          });
          feat.setId(pic.name)
          tmp_Pics.push(feat);
        }
        var iconStyle = new ol.style.Style({
          image: new ol.style.Icon(({
            anchor: [16, 32],
            size: [32, 32],
            anchorXUnits: 'pixels',
            anchorYUnits: 'pixels',
            src: 'icon.png'
          }))
        });
        global.select.getFeatures().clear();
        global.tmpFeatures = new ol.source.Vector();
        global.tmpFeatures.addFeatures(tmp_Pics);
        global.essai = new ol.layer.Vector({
        source : global.tmpFeatures,
        projection: 'EPSG:3857',
        style: [iconStyle]
        });
        //Initialisation Modify Interaction
        global.modify = new ol.interaction.Modify({
          features: global.select.getFeatures()
        });
        global.map.getLayers().clear();
        global.map.addLayer(new ol.layer.Tile({
          source: new ol.source.OSM({layer: 'sat'})
        }));
        global.map.addLayer(global.essai);
        global.map.addInteraction(global.select);
        global.map.addInteraction(global.modify);
      }
  },
  convertStringToCoord : function(stringCoord){
      var pos = stringCoord.indexOf(','),
        x = parseFloat(stringCoord.substring(0,pos)),
        y = parseFloat(stringCoord.substring(pos+1,stringCoord.length));
        return [x,y];
  },
  updateCoordinates : function(coord){
        if(Session.get('currentPics') !== ""){
          var tab = Session.get('uploadedEXIF');
          for(i in tab){
            var name = tab[i].name
            if(name === Session.get('currentPics')){
              var picsToChange = tab[i];
              updateCoord.call(tab[i], coord);
              Session.set('uploadedEXIF',tab);
            }
          }
        }
  },
  getCurrentPics: function(bool){
      var name = Session.get('currentPics');

      if(name !== ""){
        tab = Session.get('uploadedEXIF');
        for(i in tab){
          var name = tab[i].name
          if(name === Session.get('currentPics')){
            if(!bool){
              //Return only object
              return tab[i];
            } else{
              //Return Object AND index
              return [tab[i],i];
            }
          }
        }
      }
    },
    updatePhoto : function(name,desc,date){
      var res = global.getCurrentPics(true),
        pic = res[0],
        index = res[1],
        tab = Session.get('uploadedEXIF');
        updateNameAndDescription.call(tab[index],name,desc,date);
        Session.set('uploadedEXIF',tab);
        var feat = global.tmpFeatures.getFeatureById(pic.name);
        feat.setId(name);
    },
    parseDateAndTime : function(exifDate){
    //TODO : Parser date de l'exif pour mettre à jour le DatePicker
    var tab=[];
    console.log(exifDate);

    //Case issue with EXIF
    if(exifDate=== undefined){
      return new Date(1900,01,01,00,00,00);
    };

    tab.push(exifDate.substring(0,4));
    exifDate = exifDate.substring(5,exifDate.length);
    //i<4 because only 4 : in ExifDate
    for(var i = 0;i<5;i++){

        var index = exifDate.indexOf(':');

        if(i===1){
          index = exifDate.indexOf(' ');
        } else if( i === 4){
          tab.push(exifDate);
        }
        if(exifDate.substring(0,index) !==""){
          tab.push(exifDate.substring(0,index));
          exifDate = exifDate.substring(index+1,exifDate.length);
        }
      };

      return new Date(tab[0],tab[1],tab[2],tab[3],tab[4],tab[5]);
    }
};
