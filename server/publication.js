//publication des données pour le client
Meteor.publish('photos', function () {
    return DBPhotos.find();
});

Meteor.publish('files-images', function () {
    return Files.find();
});

Meteor.publish('files-thumbs', function () {
    return Thumbs.find();
});

Meteor.publish('books', function () {
    return Books.find();
});
