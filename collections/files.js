////////////////////////////////////////////////////////////////
///////////////////Collection pour le stockage de fichiers//////
////////////////////////////////////////////////////////////////

//Storing de thumbnails
var ImagesThumbsStore = new FS.Store.FileSystem('thumbs', {
    transformWrite : function(fileObj, readStream, writeStream) {
          // Transform the image into a 300x300px thumbnail
          gm(readStream, fileObj.name()).resize('300', '300').stream().pipe(writeStream);
        }
});
//Storing d'images tailles reelles
var ImagesStore = new FS.Store.FileSystem('upload');
Thumbs = new FS.Collection('files-thumbs', {
    stores: [ImagesThumbsStore],
    filter: {
        allow: { //Filter : only images allow in this DB
          contentType: ['image/*']
        }
    }
});
Files = new FS.Collection('files-images', {
    stores: [ImagesStore],
    filter: {
        allow: { //Filter : only images allow in this DB
          contentType: ['image/*']
        }
    }
});


/*Files.allow({
    insert: function (userId, fileObj) {
        // only allow posting if you are logged in
        return true;
    },
    update: function (userId, fileObj) {
        // only allow posting if you are logged in
        return !!userId;
    },
    remove: function (userId, fileObj) {
        // only allow posting if you are logged in
        return !!userId;
    },
    download: function (userId, fileObj) {
        // only allow posting if you are logged in
        return !!userId;
    },
    fetch: []
});

Thumbs.allow({
    insert: function (userId, fileObj) {
        // only allow posting if you are logged in
        return true;
    },
    update: function (userId, fileObj) {
        // only allow posting if you are logged in
        return !!userId;
    },
    remove: function (userId, fileObj) {
        // only allow posting if you are logged in
        return !!userId;
    },
    download: function (userId, fileObj) {
        // only allow posting if you are logged in
        return !!userId;
    },
    fetch: []
});*/
